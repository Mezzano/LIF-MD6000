/*
 * FPGA Manager Driver for Lattice iCE40.
 *
 *  Copyright (c) 2016 Joel Holdsworth
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 */

#include <linux/of.h>
#include <linux/module.h>	// included for all kernel modules
#include <linux/kernel.h>	// included for KERN_INFO
#include <linux/init.h>		// included for __init and __exit macros
#include <linux/fpga/fpga-mgr.h>

static int __init fpga_test_init(void)
{
	/* device node that specifies the FPGA manager to use */
	struct device_node *mgr_node;

	struct fpga_image_info *info;

	int ret;
	struct fpga_manager *mgr;

	printk(KERN_INFO "Hello world!\n");

	mgr_node = of_find_node_by_name(NULL, "fpga");
	if (!mgr_node) {
	   printk(KERN_INFO "Failed to find node\n");
	   return 0;
	}	

	/* Get exclusive control of FPGA manager */
	mgr = of_fpga_mgr_get(mgr_node);
	if (IS_ERR(mgr)) {
		printk(KERN_INFO "Failed to get FPGA manager\n");
               return PTR_ERR(mgr);
	}

	info = fpga_image_info_alloc(&mgr->dev);
	info->firmware_name = "lvds_to_mipi_impl1.bit";

	/* Get the firmware image (path) and load it to the FPGA */
	ret = fpga_mgr_load(mgr, info);
	if (ret) {
	   printk(KERN_INFO "Failed to load FW\n");
	   fpga_mgr_put(mgr);
	   return 0;
	}

	/* Release the FPGA manager */
	fpga_mgr_put(mgr);

	return 0;	// Non-zero return means that the module couldn't be loaded.
}

static void __exit fpga_test_cleanup(void)
{
	printk(KERN_INFO "Cleaning up module.\n");
}

module_init(fpga_test_init);
module_exit(fpga_test_cleanup);

MODULE_AUTHOR("Joel Holdsworth <joel@airwebreathe.org.uk>");
MODULE_DESCRIPTION("A simple FPGA test module");
MODULE_LICENSE("GPL v2");
