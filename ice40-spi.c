/*
 * FPGA Manager Driver for Lattice iCE40.
 *
 *  Copyright (c) 2016 Joel Holdsworth
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This driver adds support to the FPGA manager for configuring the SRAM of
 * Lattice iCE40 FPGAs through slave SPI.
 */

#include <linux/fpga/fpga-mgr.h>
#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/spi/spi.h>
#include <linux/stringify.h>
#include <linux/delay.h>
#include <linux/string.h>

#define ICE40_SPI_MAX_SPEED 25000000 /* Hz */
#define ICE40_SPI_MIN_SPEED 1000000 /* Hz */

#define ICE40_SPI_RESET_DELAY 1 /* us (>200ns) */
#define ICE40_SPI_HOUSEKEEPING_DELAY 1200 /* us */

#define ICE40_SPI_NUM_ACTIVATION_BYTES DIV_ROUND_UP(49, 8)

#define SPI_BUS 1
#define SPI_BUS_CS 0 
#define SPI_BUS_SPEED 1000000

const char this_driver_name[] = "ice40-spi";

struct ice40_fpga_priv {
	struct spi_device *dev;
	struct gpio_desc *pwdn;
	struct gpio_desc *cdone;
	struct gpio_desc *reset;
};

static enum fpga_mgr_states ice40_fpga_ops_state(struct fpga_manager *mgr)
{
	struct ice40_fpga_priv *priv = mgr->priv;

	return gpiod_get_value(priv->cdone) ? FPGA_MGR_STATE_OPERATING :
		FPGA_MGR_STATE_UNKNOWN;
}

static int ice40_fpga_ops_write_init(struct fpga_manager *mgr,
				     struct fpga_image_info *info,
				     const char *buf, size_t count)
{
	const u8 isc_enable[] = {0xC6, 0x00, 0x00, 0x00};
	const u8 isc_erase[] = {0x0E, 0x00, 0x00, 0x00};
	u8 rcv[31];

	struct ice40_fpga_priv *priv = mgr->priv;
	struct spi_device *dev = priv->dev;

	memset(rcv, 0, sizeof rcv);
	spi_write_then_read(dev, isc_enable, sizeof isc_enable, rcv, 0);
	mdelay(1);
	memset(rcv, 0, sizeof rcv);
	spi_write_then_read(dev, isc_erase, sizeof isc_enable, rcv, 0);
	return 0;
}

static int ice40_fpga_ops_write(struct fpga_manager *mgr,
				const char *buf, size_t count)
{
	const u8 lsc_init[] = {0x46, 0x00, 0x00, 0x00};
	const u8 lsc_bitstream_burst[] = {0x7A, 0x00, 0x00, 0x00};
	const u8 lsc_read_status[] = {0x3C, 0x00, 0x00, 0x00};

	struct ice40_fpga_priv *priv = mgr->priv;
        struct spi_device *dev = priv->dev;
	u8 rcv[31];
	char* msg = kmalloc(count+4,GFP_KERNEL);

	mdelay(200);
	spi_write_then_read(dev, lsc_init, sizeof lsc_init, rcv, 0);

	memset(rcv,0,4);
        spi_write_then_read(dev, lsc_read_status, sizeof lsc_read_status, rcv, 4);
        printk("Read status bit: %x %x %x %x\n", rcv[0], rcv[1], rcv[2], rcv[3]);
        printk("DONE: %x Busy: %x Fail: %x\n", rcv[2] & 1, (rcv[2] >> 4) & 1, (rcv[2] >> 5) & 1);

	if (msg != NULL) {
		memcpy(msg, lsc_bitstream_burst, 4);
		memcpy(msg+4, buf, count);
	}
	else {
		printk(KERN_ALERT "msg is NULL\n");
		return 0;
	}

	spi_write_then_read(dev, msg, count+4, rcv, 0);
	kfree(msg);
	printk("End of ice40_fpga_ops_write\n");
	return 0;
}

static int ice40_fpga_ops_write_complete(struct fpga_manager *mgr,
					 struct fpga_image_info *info)
{
	const u8 lsc_read_status[] = {0x3C, 0x00, 0x00, 0x00};
	const u8 isc_disable[] = {0x26, 0x00, 0x00, 0x00};
	const u8 no_op[] = {0xFF, 0xFF, 0xFF, 0xFF};
	u8 rcv[100];

	struct ice40_fpga_priv *priv = mgr->priv;
	struct spi_device *dev = priv->dev;

	printk("Start of ice40_fpga_ops_write_complete\n");
	mdelay(10);
	memset(rcv,0,4);
	spi_write_then_read(dev, lsc_read_status, sizeof lsc_read_status, rcv, 4);
        printk("Read status bit: %x %x %x %x\n", rcv[0], rcv[1], rcv[2], rcv[3]);
        printk("DONE: %x Busy: %x Fail: %x\n", rcv[2] & 1, (rcv[2] >> 4) & 1, (rcv[2] >> 5) & 1);

	spi_write_then_read(dev, isc_disable, sizeof isc_disable, rcv, 0);

	mdelay(200);
	spi_write_then_read(dev, no_op, sizeof no_op, rcv, 0);
	printk("End of ice40_fpga_ops_write_complete\n");

	return 0; 
}

static const struct fpga_manager_ops ice40_fpga_ops = {
	.state = ice40_fpga_ops_state,
	.write_init = ice40_fpga_ops_write_init,
	.write = ice40_fpga_ops_write,
	.write_complete = ice40_fpga_ops_write_complete,
};

static int add_device_to_bus(void)
{
	struct spi_master *spi_master;
	struct spi_device *spi_device;
	struct device *pdev;
	char buff[64];
	int status = 0;

	spi_master = spi_busnum_to_master(SPI_BUS);
	if (!spi_master) {
		printk(KERN_ALERT "spi_busnum_to_master(%d) returned NULL\n", SPI_BUS);
		printk(KERN_ALERT "Missing modprobe omap2_mcspi?\n");
		return -1;
	}

	spi_device = spi_alloc_device(spi_master);
	if (!spi_device) {
		put_device(&spi_master->dev);
		printk(KERN_ALERT "spi_alloc_device() failed\n");
		return -1;
	}

	spi_device->chip_select = SPI_BUS_CS;

	/* Check whether this SPI bus.cs is already claimed */
	snprintf(buff, sizeof(buff), "%s.%u", dev_name(&spi_device->master->dev), spi_device->chip_select);
	pdev = bus_find_device_by_name(spi_device->dev.bus, NULL, buff);
	if (pdev) {
		spi_dev_put(spi_device);
		if (pdev->driver && pdev->driver->name && strcmp(this_driver_name, pdev->driver->name)) {
			printk(KERN_ALERT "Driver [%s] already registered for %s\n", pdev->driver->name, buff);
			status = -1;
		}
	} else {
		spi_device->max_speed_hz = SPI_BUS_SPEED;
		spi_device->mode = SPI_MODE_0;
		spi_device->bits_per_word = 8;
		spi_device->irq = -1;
		spi_device->controller_state = NULL;
		spi_device->controller_data = NULL;
		strlcpy(spi_device->modalias, this_driver_name, SPI_NAME_SIZE);

		status = spi_add_device(spi_device);
		if (status < 0) {
			spi_dev_put(spi_device);
			printk(KERN_ALERT "spi_add_device() failed: %d\n", status);
		}
	}

	put_device(&spi_master->dev);

	return status;
}

static int ice40_fpga_probe(struct spi_device *spi)
{
	struct device *dev = &spi->dev;
	struct ice40_fpga_priv *priv;
	int ret;
	const u8 activation_key[] = {0xFF, 0xA4, 0xC6, 0xF4, 0x8A};
	const u8 idcode_pub[] = {0xE0, 0x00, 0x00, 0x00};
	u8 rcv[100];
	int error;

	priv = devm_kzalloc(&spi->dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	priv->dev = spi;

	/* Check board setup data. */
	if (spi->max_speed_hz > ICE40_SPI_MAX_SPEED) {
		dev_err(dev, "SPI speed is too high, maximum speed is "
			__stringify(ICE40_SPI_MAX_SPEED) "\n");
		return -EINVAL;
	}

	if (spi->max_speed_hz < ICE40_SPI_MIN_SPEED) {
		dev_err(dev, "SPI speed is too low, minimum speed is "
			__stringify(ICE40_SPI_MIN_SPEED) "\n");
		return -EINVAL;
	}

	if (spi->mode & SPI_CPHA) {
		dev_err(dev, "Bad SPI mode, CPHA not supported\n");
		return -EINVAL;
	}

	/* Set up the GPIOs */
	priv->pwdn = devm_gpiod_get(dev, "pwdn", GPIOD_OUT_HIGH);
        if (IS_ERR(priv->pwdn)) {
                ret = PTR_ERR(priv->pwdn);
                dev_err(dev, "Failed to get PWDN GPIO: %d\n", ret);
                return ret;
        }

	priv->cdone = devm_gpiod_get(dev, "cdone", GPIOD_IN);
	if (IS_ERR(priv->cdone)) {
		ret = PTR_ERR(priv->cdone);
		dev_err(dev, "Failed to get CDONE GPIO: %d\n", ret);
		return ret;
	}

	priv->reset = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(priv->reset)) {
		ret = PTR_ERR(priv->reset);
		dev_err(dev, "Failed to get CRESET_B GPIO: %d\n", ret);
		return ret;
	}

	gpiod_set_value(priv->pwdn, 1);
	mdelay(4000);
	gpiod_set_value(priv->reset, 0);

	mdelay(1000);
	spi_write_then_read(spi, activation_key, sizeof activation_key, rcv, 0);
	gpiod_set_value(priv->reset, 1);
	mdelay(10);
	memset(rcv, 0, sizeof rcv);
	spi_write_then_read(spi, idcode_pub, sizeof idcode_pub, rcv, 4);
	dev_info(dev, "IDCODE_PUB: %x %x %x %x \n", rcv[0], rcv[1], rcv[2], rcv[3]);
	if(rcv[3] != 0x43)
		return -ENODEV;

	error = add_device_to_bus();
	if (error < 0) {
		printk(KERN_ALERT "add_device_to_bus() failed\n");
		printk(KERN_ALERT "error: %i\n", error);
	}

	/* Register with the FPGA manager */
	return fpga_mgr_register(dev, "Lattice iCE40 FPGA Manager",
				 &ice40_fpga_ops, priv);
}

static int ice40_fpga_remove(struct spi_device *spi)
{
	fpga_mgr_unregister(&spi->dev);
	return 0;
}

static const struct of_device_id ice40_fpga_of_match[] = {
	{ .compatible = "lattice,ice40-fpga-mgr", },
	{},
};
MODULE_DEVICE_TABLE(of, ice40_fpga_of_match);

static struct spi_driver ice40_fpga_driver = {
	.probe = ice40_fpga_probe,
	.remove = ice40_fpga_remove,
	.driver = {
		.name = "ice40spi",
		.of_match_table = of_match_ptr(ice40_fpga_of_match),
	},
};

module_spi_driver(ice40_fpga_driver);

MODULE_AUTHOR("Joel Holdsworth <joel@airwebreathe.org.uk>");
MODULE_DESCRIPTION("Lattice iCE40 FPGA Manager");
MODULE_LICENSE("GPL v2");
